package com.fliife.randomwallpaper;

import android.app.PendingIntent;
import android.app.WallpaperManager;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.RemoteViews;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ExecutionException;

/**
 * Implementation of App Widget functionality.
 */
public class RandomWallpaperWidget extends AppWidgetProvider {
    public static String CHANGE_WALLPAPER_ACTION = "ChangeWallpaper";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction().equals(CHANGE_WALLPAPER_ACTION)) {
            initializeTransition(context);
        }
    }
    public void initializeTransition(Context context) {
        setAsWallpaper(context, getImageUrl("https://source.unsplash.com/random#" + Math.floor(Math.random() * 1000)));
    }

    public String getImageUrl(final String originalUrl) {
        String newURL = null;
        try {
            newURL = new GetRedirectedUrl().execute(originalUrl).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return newURL;
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {


        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.random_wallpaper_widget);


        Intent intent = new Intent(context, RandomWallpaperWidget.class);
        intent.setAction(CHANGE_WALLPAPER_ACTION);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        views.setOnClickPendingIntent(R.id.imageButton, pendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    public Bitmap getBitmapFromURL(String src) {
        Bitmap bitmap = null;
        try {
            bitmap = new GetBitmapFromUrl().execute(src).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public void setAsWallpaper(Context ctx, String Url){
        WallpaperManager m = WallpaperManager.getInstance(ctx);
        Bitmap bm = getBitmapFromURL(Url);
        try {
            m.setBitmap(bm);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class GetRedirectedUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String newUrl = "";
            try {
                URL url = new URL(params[0]);
                URLConnection con = url.openConnection();
                con.connect();
                InputStream is = con.getInputStream();
                newUrl = con.getURL().toString();
                is.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return newUrl;
        }
    }

    private class GetBitmapFromUrl extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                // Log exception
                return null;
            }
        }
    }
}

