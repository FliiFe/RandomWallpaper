package com.fliife.randomwallpaper;

import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    public String currentURL = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
            }
        });
        refresh();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public String getImageUrl(final String originalUrl) {
        String newURL = null;
        try {
            newURL = new GetRedirectedUrl().execute(originalUrl).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return newURL;
    }

    public void refresh(){
        ImageView iv = (ImageView) findViewById(R.id.imageView);
        currentURL = "https://source.unsplash.com/random#" + Math.floor(Math.random() * 1000);
        currentURL = getImageUrl(currentURL);

        UrlImageViewHelper.setUrlDrawable(iv, currentURL);
    }

    public Bitmap getBitmapFromURL(String src) {
        Bitmap bitmap = null;
        try {
            bitmap = new GetBitmapFromUrl().execute(src).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public void setAsWallpaper(){
        WallpaperManager m = WallpaperManager.getInstance(this);
        Bitmap bm = getBitmapFromURL(currentURL);
        try {
            m.setBitmap(bm);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            setAsWallpaper();
        }

        return super.onOptionsItemSelected(item);
    }

    private class GetRedirectedUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String newUrl = "";
            try {
                URL url = new URL(params[0]);
                URLConnection con = url.openConnection();
                con.connect();
                InputStream is = con.getInputStream();
                newUrl = con.getURL().toString();
                is.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return newUrl;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private class GetBitmapFromUrl extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                // Log exception
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
        }
        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
